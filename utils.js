// Require the use of IOTA library
const Iota = require('@iota/core')
const Converter = require('@iota/converter')
// const ExtractJSON = require('@iota/extract-json')
const iotaAreaCodes = require('@iota/area-codes')
const iota = Iota.composeAPI({ provider: 'https://nodes.devnet.iota.org:443' })

const axios = require('axios')

const googleApi = require('./googleApi.json')
const apiKey = googleApi.key

const cassandra = require('cassandra-driver')
const client = new cassandra.Client({
  contactPoints: ['127.0.0.1'],
  localDataCenter: 'datacenter1'
})


// item creator Wizard

// itemName: value.itemName,
// description: value.description,
// OPTIONAL listDays: value.listDays,
// OPTIONAL AUTO address: this.props.myPlaces.properties.label,
// AUTO lng: this.props.myPlaces.geometry.coordinates[0],
// AUTO lat: this.props.myPlaces.geometry.coordinates[1],
// AUTO_TELEGRAM_USER user: Meteor.userId(),
// AUTO_TELEGRAM_USER username: Meteor.user().username,
// AUTO_TELEGRAM_CAMERA imageId: this.props.imageId,
// OPTIONAL endDate: new Date(Date.now() + value.listDays * 24*60*60*1000)

const opts = {
  reply_markup: JSON.stringify({
    keyboard: [
      [{ text: 'Location', request_location: true }]
    ],
    resize_keyboard: true,
    one_time_keyboard: true
  })
}

let umlautMap = {
  '\u00dc': 'UE',
  '\u00c4': 'AE',
  '\u00d6': 'OE',
  '\u00fc': 'ue',
  '\u00e4': 'ae',
  '\u00f6': 'oe',
  '\u00df': 'ss',
}

const replaceUmlaute = (str) => {
  return str
    .replace(/[\u00dc|\u00c4|\u00d6][a-z]/g, (a) => {
      var big = umlautMap[a.slice(0, 1)];
      return big.charAt(0) + big.charAt(1).toLowerCase() + a.slice(1);
    })
    .replace(new RegExp('['+Object.keys(umlautMap).join('|')+']',"g"),
      (a) => umlautMap[a]
    )
}

const distance = (lat1, lon1, lat2, lon2) => {
  if ((lat1 === lat2) && (lon1 === lon2)) {
    return 0
  } else {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
    if (dist > 1) {
      dist = 1
    }
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    dist = dist * 1.609344
    return dist
  }
}



async function iacToAddress(iac) {
  //iac to lat long
  const locObj = iotaAreaCodes.decode(iac);
  // latitude longitude
  let res = null
  let address = null
  const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${locObj.latitude},${locObj.longitude}&key=${apiKey}`
  const opts = {
    method: 'GET',
    headers: { 'content-type': 'application/json; charset=UTF-8' },
    url,
  };
  try {
    res = await axios(opts)
    address = res.data.results[0].formatted_address
  } catch(e) {
    console.log('error:', e)
  }

  return address
}

async function geoToAddress(lat, lng) {
  let res = null
  let address = null
  const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${apiKey}`
  const opts = {
    method: 'GET',
    headers: { 'content-type': 'application/json; charset=UTF-8' },
    url,
  };
  try {
    res = await axios(opts)
    address = res.data.results[0].formatted_address
  } catch(e) {
    console.log('error:', e)
  }

  return address
}

async function addressToIac(address) {
  let res = null
  const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${apiKey}`
  const opts = {
    method: 'GET',
    headers: { 'content-type': 'application/json; charset=UTF-8' },
    url,
  };

  let iac = null
  try {
    res = await axios(opts)
    console.log(res)
    const formattedAddress = res.data.results[0].formatted_address
    const locationObj = res.data.results[0].geometry.location
    iac = iotaAreaCodes.encode(locationObj.lat, locationObj.lng);
  } catch(e) {
    console.log('error:', e)
  }
  return iac
}

const storeTransaction = async array => {
  const queries = array.map(({ tx_id, iac, message }) => {
    if (!message) {
      message = "ZZZZZZZZZ"
    }
    const query = `INSERT INTO txDB.transaction (tx_id, iac, pair0, pair1, pair2, pair3, pair4, message) VALUES (?,?,?,?,?,?,?,?);`
    const params = [
      tx_id,
      iac,
      iac.slice(0, 2),
      iac.slice(2, 4),
      iac.slice(4, 6),
      iac.slice(6, 8),
      iac.slice(9, 11),
      message
    ]
    return { query, params }
  })
  try {
    return await client.batch(queries, { prepare: true })
  } catch (e) {
    return Error(e)
  }
}

// const queryResult = async txId => {
//   let query = `SELECT tx_id, iac, message FROM txDB.transaction WHERE tx_id = `
//   query += txId + ''
// }
//

const getFoodBaskets = async () => {
  let result = await axios.get('https://foodsharing.de/xhr.php?f=loadMarker&types%5B%5D=baskets&types%5B%5D=fairteiler')
  return result.data.baskets
}

const getFoodBasketById = async (id) => {
  let result = await axios.get('https://foodsharing.de/xhrapp.php?app=basket&m=bubble&app=basket&id=' + id)
  let s = JSON.stringify(result.data)
          .replace(/\\n/g, '')
          .replace(/\\t/g, '')
          .replace(/\\/g, '')
          .replace(/u00e4/g, 'ä').replace(/u00f6/g, 'ö').replace(/u00fc/g, 'ü')
          .replace(/u00c4/g, 'Ä').replace(/u00d6/g, 'Ö').replace(/u00dc/g, 'Ü')
          .replace(/u00df/g, 'ß').replace(/<br\s*[\/]?>/gi, "")

  let startIndex = s.indexOf("Beschreibung")
      // Beschreibung</label><div class="element-wrapper">
  let endIndex = s.indexOf("</div><input")
  let basketData = s.substring(startIndex + 49, endIndex)
  return basketData
}

const queryTransactions = async iac => {
  // if (!iotaAreaCodes.isValidPartial(iac)) {
  //   throw new Error('The IOTA Area Code must be a full partial with 11 characters or less')
  // }
  const iacArray = iac.split('')
  const pairs = iacArray.indexOf('A') / 2

  // Beginning query
  let query = `SELECT tx_id, iac, message FROM txDB.transaction WHERE`

  // Construct query
  Array(pairs)
    .fill()
    .map((_, i) => {
      query = query + ` pair${i} = '${iacArray[i * 2] + iacArray[i * 2 + 1]}'`
      if (pairs - 1 != i) {
        query = query + ' AND'
      } else {
        query = query + ';'
      }
    })
  console.log(query)
  let res = await client.execute(query)
  delete res.info

  return {
    success: true,
    items: res.rows
  }
}

const tangleStore = (item, iac) => {
  const seed =
      'PEEOTSEITFEVEWCWBTSIZM9NKRGJEIMXTULBACGFRQK9IMGICLBKW9TTEVSDQMGWKBXPVCBMMCXWMNPDX'

  // Create a variable for the address we will send too
  const address =
      'IOTATANGLESHARETEST99999999999999999999999999999999999999999999999999999999999999'

  // item.name, item.description
  const message = Converter.asciiToTrytes(JSON.stringify(item))

  const transfers = [
    {
      value: 0,
      address: address, // Where the data is being sent
      tag: iac,
      message: message // The message converted into trytes
    }
  ]

  iota
    .prepareTransfers(seed, transfers)
    .then(trytes => iota.sendTrytes(trytes, 3, 9))
    .then(bundle => {
      console.log('Transfer successfully sent')
      bundle.map(tx => console.log(tx))
    })
    .catch(err => {
      console.log(err)
    })
}

module.exports = { geoToAddress, getFoodBaskets, getFoodBasketById, opts, replaceUmlaute, distance, queryTransactions, storeTransaction, tangleStore, iacToAddress, addressToIac}
