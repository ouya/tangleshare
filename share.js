const iotaAreaCodes = require('@iota/area-codes')
const Converter = require('@iota/converter')
const {
  geoToAddress,
  getFoodBaskets,
  getFoodBasketById,
  opts,
  replaceUmlaute,
  distance,
  queryTransactions,
  tangleStore,
  iacToAddress,
  addressToIac
} = require('./utils')
const {
  subscribeToZMQ
} = require('./zmq')

const Telegraf = require('telegraf')
const Markup = require('telegraf/markup') // Get the markup module
const Stage = require('telegraf/stage')
const session = require('telegraf/session')
const Extra = require('telegraf/extra')

const telegramEssentials = require('./telegram.json')
const bot = new Telegraf(telegramEssentials.key)
const stage = new Stage()
const WizardScene = require('telegraf/scenes/wizard')

bot.use(session({
  inlineMode: true
}))
bot.use(stage.middleware())
bot.use(Telegraf.log())

subscribeToZMQ()

// ############################### MAIN ###############################

bot.command('start', ({
  reply
}) => {
  return reply('⭐Welcome to tangleShare⭐! ' + "\n" + "\n" +
    'How to start sharing? ' + "\n" +
    '1. BEFORE you start: Click the "📌 Set location" button below or send a different location' + "\n" +
    '2. Take a new 📷 picture of your item OR send a 🖼️ picture OR just 📝 tell me more about it',
    Markup.keyboard([
      ['🔍 Search Space'],
      ['📦 Share item'],
      [Markup.locationRequestButton('📌 Use current location')],
      ['🗺 Use other location']
    ]).oneTime().resize().extra()
  )
})

bot.command('help', ({
  reply
}) => {
  return reply('🤔 Welcome to tangleShare help❓ ' + "\n" + "\n" +
    '🙋 Please just /start the bot, it should be easy to get along with')
})
bot.command('settings', ({
  reply
}) => {
  return reply('🔧 Welcome to tangleShare settings ' + "\n" + "\n" +
    'Currently under construction ... 🚧🏗️ ... v0.1')
})


const SHARE_ITEM = new WizardScene(
  'SHARE_ITEM',
  ctx => { // noPhoto query?
    if (ctx.session.item && ctx.session.item.photodescription) {
      ctx.reply('Do you want to add item description? (a must yes :))', Markup
        .keyboard([
          ['yes, sure!']
        ]).oneTime().resize().extra()
      )
    } else {
      ctx.reply('🔍 Add photo?', Markup
        .keyboard([
          ['yes'],
          ['no']
        ]).oneTime().resize().extra()
      )
    }
    return ctx.wizard.next()
  },
  ctx => {
    if (ctx.message.text == 'yes') {
      // end if user wants to take a picture
      ctx.reply('Photo required! Press 📎 / 📷 icon below the command line!')
      return ctx.scene.leave()
    } else {
      if (ctx.session.location) {
        ctx.reply('Item name: ')
        return ctx.wizard.next()
      } else {
        ctx.reply(`Please give location first!`)
        ctx.reply(`/start`) // TODO automate this?
        return ctx.scene.leave()
      }
    }
  },
  ctx => {
    if (!ctx.session.item) ctx.session.item = {} // init if not existent
    ctx.session.item.name = replaceUmlaute(ctx.message.text)
    // .replaceAll("[!/^[\x00-\x7F]*$/]", "")
    ctx.reply(`Item description:`)
    return ctx.wizard.next()
  },
  ctx => {
    ctx.session.item.description = replaceUmlaute(ctx.message.text)
    console.log(ctx.session.item.description)
    // PUBLISH OR NOT!!!???
    ctx.reply('❔ Publish Item 📦 to Tangle ❔', Markup
      .keyboard([
        ['yes'],
        ['⛔no']
      ]).oneTime().resize().extra()
    )
    return ctx.wizard.next()
  },
  ctx => {
    if (ctx.message.text == 'yes') {
      ctx.session.item.userId = ctx.chat.id // save userId
      const iac = iotaAreaCodes.encode(ctx.session.location.latitude, ctx.session.location.longitude)
      tangleStore(ctx.session.item, iac)
      ctx.reply('💎 Congratulations. Item shared on the IOTA tangle!' + "\n" + "\n" +
        'Continue with 1) 🔍 searching items or 2) 📦 publishing more', Markup
        .keyboard([
          ['🔍 Search Space'],
          ['📦 Share item']
        ]).oneTime().resize().extra()
      )
    } else {
      ctx.reply('⚠️ Warning. ❌📦 Item cancelled!' + "\n" + "\n" +
        'Continue with 1) 🔍 searching items or 2) 📦 publishing more', Markup
        .keyboard([
          ['🔍 Search Space'],
          ['📦 Share item']
        ]).oneTime().resize().extra()
      )
    }
    ctx.session.item = {}
    return ctx.scene.leave()
  }
)

const SEARCH = new WizardScene(
  'SEARCH',
  ctx => {
    if (ctx.session.location) {
      // ctx.reply(`Your location is:` + ctx.session.location.latitude + `/` + ctx.session.location.longitude)
      // ctx.reply(`\n GridSize: Type X for xlarge (2200km), L for (110km), M for (5.5km), S for (275m)`)
      ctx.reply('🔍 Define Search Radius - small (150m), medium(3km), large(50km), xlarge(1000km)', Markup
        .keyboard([
          ['small'],
          ['medium'],
          ['large'],
          ['xlarge']
        ]).oneTime().resize().extra()
      )

      return ctx.wizard.next()
    } else {
      ctx.reply(`Please give location first!`)
      ctx.reply('/start')
      return ctx.scene.leave()
    }
  },
  async (ctx) => {
    ctx.reply(`\n Searching ... in ` + ctx.message.text + ' size area')

    switch (ctx.message.text) {
      case 'small':
        ctx.session.gridChar = 8;
        break
      case 'medium':
        ctx.session.gridChar = 6;
        break
      case 'large':
        ctx.session.gridChar = 4;
        break
      case 'xlarge':
        ctx.session.gridChar = 2;
        break
      default:
        ctx.reply(`\n Max grid selected `);
        ctx.session.gridChar = 2;
        break
    }
    let encodeIAC = iotaAreaCodes.encode(parseFloat(ctx.session.location.latitude), parseFloat(ctx.session.location.longitude))
    let iac = iotaAreaCodes.setPrecision(encodeIAC, ctx.session.gridChar)
    // bugfix special case
    if (ctx.session.gridChar == 8) {
      iac = iac.substr(0, 8) + 'AA9'
    }

    let response = await queryTransactions(iac)
    // // get all foodBaskets
    let foodBaskets = await getFoodBaskets()
    foodBaskets = foodBaskets.map((basket) => {
      basket.description = 'Rescue food! - click on link to see description: ' + 'https://foodsharing.de/essenskoerbe/' + basket.id
      basket.photoUrl = 'https://foodsharing.de/img/foodloob.gif'
      return basket
    })


    if (foodBaskets.length > 0) {
      let radius = 150 // default = large(50km)
      ctx.reply(`Some Food item(s) found:`)
      for (var i = 0; i < foodBaskets.length; i++) {
        let dist = distance(ctx.session.location.latitude, ctx.session.location.longitude, foodBaskets[i].lat, foodBaskets[i].lon)
        if (dist < radius) { // radius 50km
          let foodAddress = await geoToAddress(foodBaskets[i].lat, foodBaskets[i].lon)

          let foodInfo = '------------------------------------' + "\n" +
            foodBaskets[i].description + "\n" +
            'Location (' + dist.toFixed(2) + ' km):' + foodAddress

          ctx.replyWithPhoto({
            url: foodBaskets[i].photoUrl
          }, {
            caption: foodInfo
          })
        }
      }

    }

    if (response.success) {
      ctx.reply(`${response.items.length} item(s) found:`)
      for (var i = 0; i < response.items.length; i++) {
        let outputLine = Converter.trytesToAscii(response.items[i].message + '9')
        let outputJson = JSON.parse(outputLine.replace(/[\u0000-\u0019]+/g, ''))
        let photoId = outputJson.photoId
        let ownerId = outputJson.userId
        let chat = await ctx.telegram.getChat(ownerId)
        let areaCode = iotaAreaCodes.decode(response.items[i].iac)
        let dist = distance(ctx.session.location.latitude, ctx.session.location.longitude, areaCode.latitude, areaCode.longitude)
        if (outputLine.length < 4096) {
          // photo available?
          let itemAddress = await iacToAddress(response.items[i].iac)
          let generalInfoText = '------------------------------------' + "\n" +
            outputJson.name + ' / ' + outputJson.description + "\n" +
            'Location (' + dist.toFixed(2) + ' km):' + itemAddress + "\n" +
            'Contact owner: ' + "@" + chat.username

          if (photoId) {
            let fileHandle = 'https://api.telegram.org/bot' + telegramEssentials.key + '/getFile?file_id=' + photoId
            let fileFetch = await fetch(fileHandle).then((response) => response.json())
            let photoUrl = 'https://api.telegram.org/file/bot' + telegramEssentials.key + '/' + fileFetch.result.file_path

            let photoCaption = outputJson.photodescription + "\n" + generalInfoText
            ctx.replyWithPhoto({
              url: photoUrl
            }, {
              caption: photoCaption
            })
          } else {
            ctx.reply("--- no picture available" + "\n" + generalInfoText)
          }

        } else {
          ctx.reply('Message too long for this dataset - max 4096 characters')
        }

      }

    }
    return ctx.scene.leave()
  }
)

// Scene registration
stage.register(SHARE_ITEM)
stage.register(SEARCH)

bot.launch()

// ############################### INLINE ###############################

let inlineresults = []
bot.on('inline_query', async (ctx) => {
  if (ctx.inlineQuery.query === 'start' || ctx.inlineQuery.query === 'menu' || ctx.inlineQuery.query === 'help') {
    ctx.telegram.sendMessage(ctx.from.id, 'Type /start to start bot').then((result) => {}).catch((err) => {
      console.log(err)
    })
  }

  if (ctx.inlineQuery.location) {
    let encodeIAC = iotaAreaCodes.encode(parseFloat(ctx.inlineQuery.location.latitude), parseFloat(ctx.inlineQuery.location.longitude))
    let iac = iotaAreaCodes.setPrecision(encodeIAC, 2)
    let response = await queryTransactions(iac)
    if (response.success) {
      // remove non-printable and other non-valid JSON chars
      // s = s.replace(/[\u0000-\u0019]+/g, '')
      // console.log(JSON.parse(Converter.trytesToAscii(response.items[0].message + '9').replace(/[\u0000-\u0019]+/g, '')))

      //https://api.telegram.org/bot<bot_token>/getFile?file_id=the_file_id
      //     "ok" : true,
      // "result" : {
      //     "file_id" : "XXXX",
      //     "file_size" : 27935,
      //     "file_path" : "photo\/file_1.jpg"
      // }
      // https://api.telegram.org/file/bot<token>/<file_path>
      const asyncFetch = async item => {
        let fileHandle = 'https://api.telegram.org/bot' + telegramEssentials.key + '/getFile?file_id=' + JSON.parse(Converter.trytesToAscii(item.message + '9').replace(/[\u0000-\u0019]+/g, '')).photoId
        return await fetch(fileHandle).then((response) => response.json())
      }

      const getData = async () => {
        return await Promise.all(response.items.map(item => asyncFetch(item)))
      }

      const data = await getData()
      response.items = response.items.map((item, index) => {
        if (data[index].result) {
          item.photoId = data[index].result.file_path
        } else {
          item.photoId = null
        }

        return item
      })

      inlineresults = response.items.map((item) => {
        let itemMsg = JSON.parse(Converter.trytesToAscii(item.message + '9').replace(/[\u0000-\u0019]+/g, ''))
        let imageURL = 'https://shenandoahcountyva.us/bos/wp-content/uploads/sites/4/2018/01/picture-not-available-clipart-12.jpg'
        let username = 'CHAT with OWNER'
        let itemDistance = distance(ctx.inlineQuery.location.latitude, ctx.inlineQuery.location.longitude, iotaAreaCodes.decode(item.iac).latitude, iotaAreaCodes.decode(item.iac).longitude).toFixed(2)
        console.log(item.photoId)
        let photo_url = ((item.photoId) ? 'https://api.telegram.org/file/bot' + telegramEssentials.key + '/' + item.photoId : 'https://shenandoahcountyva.us/bos/wp-content/uploads/sites/4/2018/01/picture-not-available-clipart-12.jpg')
        let markdown = `**${itemMsg.name}**
  > ${itemMsg.description}['Click to download image:' ](${photo_url})
  > Click to -> **[${username}](tg://user?id=${itemMsg.userId})**`
        return {
          type: 'article',
          dist: itemDistance,
          userId: itemMsg.userId,
          id: item.tx_id.slice(0, 7),
          iac: item.iac,
          thumb_url: photo_url,
          title: itemMsg.name,
          description: itemMsg.description + ' : ' + itemDistance + ' km away' ,
          reply_markup: {
            inline_keyboard: [
              [{
                text: 'Like this item 👍',
                callback_data: 'likeitem'
              }]
            ]
          },
          input_message_content: {
            message_text: markdown,
            parse_mode: 'Markdown'
          }
        }
      })

      // // get all foodBaskets
      let foodBaskets = await getFoodBaskets()
      foodBaskets = foodBaskets.map((basket) => {
        basket.description = 'Rescue food! - click on link to see description: ' + 'https://foodsharing.de/essenskoerbe/' + basket.id
        basket.photoUrl = 'https://foodsharing.de/img/foodloob.gif'
        return basket
      })
      if (foodBaskets.length > 0) {
        let radius = 150 // default = large(50km)
        for (var i = 0; i < foodBaskets.length; i++) {
          let dist = distance(ctx.inlineQuery.location.latitude, ctx.inlineQuery.location.longitude, foodBaskets[i].lat, foodBaskets[i].lon)
          if (dist < radius) { // radius 50km
            let foodAddress = await geoToAddress(foodBaskets[i].lat, foodBaskets[i].lon)

            let foodInfo = '------------------------------------' + "\n" +
              foodBaskets[i].description + "\n" +
              'Location (' + dist.toFixed(2) + ' km):' + foodAddress

              let markdown = `**${foodInfo}**
              > ['Click to see details:' ](${foodBaskets[i].photoUrl})`

            let foodItem = {
                type: 'article',
                id: foodBaskets[i].id,
                title: 'Rescue food!',
                description: 'Leftovers to pick up: ' + dist.toFixed(2) + ' km away' ,
                thumb_url: 'https://foodsharing.de/img/foodloob.gif',
                input_message_content: {
                  message_text: markdown,
                  parse_mode: 'Markdown'
                }
            }
            inlineresults.push(foodItem)
          }
        }

      }

      inlineresults = inlineresults.filter(item => item.title.toUpperCase().indexOf(ctx.inlineQuery.query.toUpperCase()) !== -1)
      inlineresults.sort((a, b) => a.dist - b.dist)
    }
  }
  else {
    // noLocation message item shown
    let noLocationMessage = {
      type: 'article',
      id: 01234567,
      title: 'Missing Location',
      input_message_content: {
        message_text: 'NO LOCATION SET -> NO ITEMS FOUND',
        parse_mode: 'Markdown',
      }
    }
    inlineresults.push(noLocationMessage)
  }

  ctx.answerInlineQuery(inlineresults, {
    is_personal: true
  })
})

bot.action('likeitem', (ctx) =>
  console.log('like item')
)

// ############################### LISTENER ###############################
bot.hears('🔍 Search Space', ctx => ctx.scene.enter('SEARCH'))
bot.hears('📦 Share item', ctx => ctx.scene.enter('SHARE_ITEM'))
const extra = Extra.markup(Markup.inlineKeyboard([
  Markup.urlButton('Click 📎 attach symbol, select 📌 location, link to Help ❤️', 'https://core.telegram.org/blackberry/chat-media-send')
]))
bot.hears('🗺 Use other location', ctx => ctx.replyWithPhoto('http://cdn.iphonehacks.com/wp-content/uploads/2017/05/share-location-telegram-1.png', extra))
//https://core.telegram.org/blackberry/chat-media-send

bot.on('photo', (ctx) => {
  if (ctx.session.location) {
    // clear item data
    ctx.session.item = {}
    if (ctx.message.caption) {
      ctx.session.item.photodescription = replaceUmlaute(ctx.message.caption)
      ctx.session.item.userId = ctx.message.from.id
      ctx.session.item.photoId = ctx.message.photo[0].file_id
      // now get additional item information (name, description)
      ctx.scene.enter('SHARE_ITEM')
    } else {
      ctx.reply(`no caption/title for the item set! cannot share photoitem. Please retake picture`)
      ctx.reply('/start again!')
    }
  } else {
    ctx.reply(`no location set! cannot share item`)
    ctx.reply('/start again!')
  }
  // ctx.telegram.sendPhoto(ctx.message.chat.id, ctx.message.photo[0].file_id)
})

bot.on('location', (ctx) => {
  ctx.session.location = ctx.message.location
  ctx.reply('🎈 Updated Location (Lat/Lng):' + ctx.session.location.latitude.toFixed(2) + '/' + ctx.session.location.longitude.toFixed(2) + "\n" + "\n" +
    'Time to start sharing!' + "\n" +
    'Send a 📷 / 🖼️ picture to portray your item or 📝 just name it' + "\n", Markup
    .keyboard([
      ['🔍 Search Space'],
      ['📦 Share item']
    ]).oneTime().resize().extra()
  )
})



// ############## MISC ##############################
// bot.command('getContact', (ctx) => {
//   return ctx.reply('Special buttons keyboard', Extra.markup((markup) => {
//       return markup.resize()
//       .keyboard([
//         markup.contactRequestButton('contact')
//       ])
//   }))
// })
// //listens for the click on contact button
// bot.on('contact', (ctx) => {
//   console.log(ctx.update.message.contact);
// })
